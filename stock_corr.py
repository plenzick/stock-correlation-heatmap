import sys
import yfinance as yf
import pandas as pd
import datetime
import plotly.graph_objects as go

def get_close_price(stock, start_date, end_date):
	return pd.DataFrame(yf.Ticker(stock).history(start=start_date, end=end_date)['Close']).rename(columns={'Close':stock.upper()})

def createFig(corr_matrix):
    fig = go.Figure(data=go.Heatmap(z=corr_matrix.values,
                    x=corr_matrix.index.values,
                    y=corr_matrix.columns.values,
                    colorscale='thermal'))
    fig.update_xaxes(side="top")
    fig.update_layout(title='Stock Correlation Map')

    #fig.write_image("stock_heatmap.png")
    fig.write_html("stock_heatmap.html", include_plotlyjs='cdn')

def main(tickers, days):
    end_date = datetime.date.today()
    start_date = end_date - datetime.timedelta(days)
    stock_df = pd.DataFrame()
    for stock in tickers:
	    if stock_df.empty: 
	    	stock_df = get_close_price(stock, start_date, end_date)
	    else:
		    stock_df = stock_df.join(get_close_price(stock, start_date, end_date))
    corr_matrix = stock_df.corr()
    createFig(corr_matrix)

if __name__ == "__main__":
    days = int(sys.argv[-1])
    tickers = sys.argv[1:-1]
    main(tickers, days)
