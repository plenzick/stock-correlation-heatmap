# Stock Correlation Heatmap

Basic script to show correlation of stock closing prices with a heatmap

## Usage

`python stock_corr.py <ticker symbols> <# days>`

## Example Output

input: `python stock_corr.py AAPL XOM SPY SBUX 90`

![screenshot of output](example_output_screenshot.png)

Note this is just a screenshot, while actual output is an interactive html/javascript file 
